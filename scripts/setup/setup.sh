#!/bin/bash

# configure auth
setup_kubectl() {
  SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
  echo "SETUP KUBECONFIG"
  mkdir -p $HOME/.kube
  cp -rf "${SCRIPT_DIR}"/kubeconfig $HOME/.kube/config
  chmod go-r $HOME/.kube/config

  echo "SETUP CONTEXT NAMESPACE"
  #Set context namespace
  kubectl config set-context --current --namespace="${KUBES_NAMESPACE}"
}

setup_docker_registry() {
  echo "SETUP GITLAB DOCKER REGISTRY"
  kubectl delete secret gitlab-reg
  kubectl create secret --namespace "${KUBES_NAMESPACE}" docker-registry gitlab-reg --docker-server=registry.gitlab.com --docker-username=gitlab-ci-token --docker-password="${GITLAB_CI_TOKEN}" --docker-email="${GITLAB_CI_USER_EMAIL}"
  kubectl get --namespace "${KUBES_NAMESPACE}" secret gitlab-reg --output=yaml
  kubectl patch --namespace "${KUBES_NAMESPACE}" serviceaccount default -p '{"imagePullSecrets": [{"name": "gitlab-reg"}]}'
}

setup_kubectl
setup_docker_registry