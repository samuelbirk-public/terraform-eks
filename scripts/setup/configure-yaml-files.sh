#!/bin/bash

# configure auth
configure_yaml_files() {
  SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"


  echo "PROJECT-BRANCH: ${PROJECT}-${BRANCH}"

  envsubst < "$SCRIPT_DIR"/../../rails-templates/migration.yaml > k8-migration.yaml
  envsubst < "$SCRIPT_DIR"/../../rails-templates/app.yaml > k8-app.yaml
}

configure_yaml_files