#!/bin/bash

# configure auth

run_migrations() {
  kubectl delete -f k8-migration.yaml || true
  kubectl apply -f k8-migration.yaml
  JOBS=$(grep -A 2 Job k8-migration.yaml | grep "name:" | awk -F ':' '{print $2}')
  for JOB_LOCAL in ${JOBS}; do
    export JOB=${JOB_LOCAL};
    echo "JOB: '${JOB}'"
    export JOB_NAME="job/${JOB}"
    kubectl wait --for=condition=complete --timeout=300s ${JOB_NAME##*( )} # requires 1.11 kubernetes version
    if [[ $? -ne 0 ]] ; then                  # Make final determination.
        echo "Job Failed"
        exit 1
    fi
  done
}




deploy_app() {
  kubectl apply -f k8-app.yaml

  DEPLOYMENTS=$(grep -A 2 Deployment k8-app.yaml | grep "name:" | awk -F ':' '{print $2}')
  for DEPLOYMENT in ${DEPLOYMENTS}; do
    DEPLOYMENT_NAME="deploy/${DEPLOYMENT}"
    echo "DEPLOYMENT NAME: '${DEPLOYMENT_NAME}'"
    kubectl rollout status -w "${DEPLOYMENT_NAME}"
    if [[ $? -ne 0 ]] ; then                  # Make final determination.
      echo "Deployment Failed"
      exit 1
    fi
  done
}

run_migrations
deploy_app