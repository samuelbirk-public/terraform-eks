undeploy() {
  SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
  $SCRIPT_DIR/setup/setup.sh
  $SCRIPT_DIR/setup/configure-yaml-files.sh
  $SCRIPT_DIR/deploy/undeploy.sh
}
