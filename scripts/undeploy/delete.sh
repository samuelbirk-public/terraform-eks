#!/bin/bash

# configure auth
. /config/kube-deploy-configure.sh

kubectl delete -f kube-deployment.yaml

/config/kube-deploy-unconfigure.sh
