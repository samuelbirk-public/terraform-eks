deploy() {
  SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
  echo $SCRIPT_DIR
  echo 'RUNNING SETUP'
  $SCRIPT_DIR/setup/setup.sh
  echo 'CONFIGURING YAML FILES'
  $SCRIPT_DIR/setup/configure-yaml-files.sh
  echo 'DEPLOYING K8 APP'
  $SCRIPT_DIR/deploy/deploy.sh
}
deploy