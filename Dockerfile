FROM ubuntu:18.04
RUN apt-get update -y \
    && apt-get install -y \
    openssh-client \
    gnupg \
    software-properties-common \
    curl \
    unzip \
    vim \
    wget \
    git \
    build-essential \
    gettext-base

RUN mkdir /terraform-eks

WORKDIR /terraform-eks

ADD . /terraform-eks

# install terraform
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - \
    && apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" \
    && apt-get update && apt-get install terraform

# install awscli
RUN curl 'https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip' -o awscliv2.zip && unzip awscliv2.zip \
    && ./aws/install \
    && rm -rf ./aws awscliv2.zip

# install kubectl \
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.2/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl

#install helm
RUN curl -sSL https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

#install eksctl
RUN curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp \
    && mv /tmp/eksctl /usr/local/bin

RUN cat ./bashrc >> $HOME/.bashrc

RUN chmod +x scripts/*.sh
RUN chmod +x scripts/**/*.sh

WORKDIR /app
